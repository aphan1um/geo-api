import sites from './sites.js';
import crypto from 'crypto';
import express from 'express';
import winston from 'winston';
import expressWinston from 'express-winston'
import { META_INFO, API_PORT } from './meta.js'
import location from './location.js'

const app = express();

app.use(express.json());

// Thanks to https://stackoverflow.com/a/42344170
expressWinston.requestWhitelist.push('body');
expressWinston.responseWhitelist.push('body');
expressWinston.responseWhitelist.push('locals.res_headers');
expressWinston.responseWhitelist.push('locals.all_logs');

app.use(expressWinston.logger({
  transports: [
    new winston.transports.Console({
      json: true,
      colorize: true,
    })
  ]
}));

app.use((_, res, next) => {
  res.locals.all_logs = [];
  next();
});

app.use((_, res, next) => {
  res.append('X-UUID', crypto.randomUUID());
  res.locals.res_headers = res.getHeaders();
  next();
});

app.get('/meta', async (req, res) => {
  res.json(META_INFO);
});

app.post('/google', async (req, res) => {
  const result = await sites.google(req.body.query, res.locals.all_logs);
  res.json(result);
});

app.post('/google/abn', async (req, res) => {
  const result = await sites.google_abn(req.body, res.locals.all_logs);
  res.json(result);
});

app.post('/google/abn/detailed', async (req, res) => {
  const result = await sites.abn_from_name(req.body, res.locals.all_logs);
  res.json(result.chosenABR);
});

app.post('/dnb', async (req, res) => {
  const result = await sites.dunbradstreet(req.body, res.locals.all_logs);
  res.json(result);
});

app.post('/dnb/with-postcode', async (req, res) => {
  const result = await sites.dnb_with_postcode(req.body.query, req.body.postcode, res.locals.all_logs);
  res.json(result);
});

app.post('/yellow', async (req, res) => {
  const result = await sites.yellow_pages(req.body, res.locals.all_logs);
  res.json(result);
});

app.post('/abn', async (req, res) => {
  res.json(await sites.abnNumberDetailed(req.body.abnNumber));
});

app.post('/abn/search', async (req, res) => {
  const result = await sites.abnSearch(req.body.companyName, req.body.state, res.locals.all_logs);
  res.json(result);
});

app.post('/abn/from-name', async (req, res) => {
  res.json(await sites.abnDetailedFromName(req.body.companyName));
});

app.post('/location', async (req, res) => {
  const result = await location.getLocation(req.body, res.locals.all_logs);
  res.json(result);
});

app.use(expressWinston.errorLogger({
  transports: [
    new winston.transports.Console({
      json: true,
      colorize: true,
    })
  ]
}));

const server = app.listen(API_PORT, '0.0.0.0', () => {
   console.log(`[INFO] App listening at ${server.address().address}:${server.address().port}`);
});

// (async () => {
//   // const r = await sites.yellow_pages('Professional Tree Surgeons', 'Osborne Park WA')
//   // const r = await sites.google('Blackwood Pantry', 'Cronulla NSW');

//   console.log(await sites.dunbradstreet('SMP GROUP PTY LTD'));

//   // console.log(r);

//   // console.log(r);
// })();

