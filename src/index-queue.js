import PgBoss from 'pg-boss';
import fs from 'fs';
import sites from './sites.js';
import { sleep } from './util.js';
import _ from 'lodash';

const boss = new PgBoss({
  user: process.env['PG_USERNAME'],
  password: process.env['PG_PASSWORD'],
  host: process.env['PG_HOST'],
  port: Number(process.env['PG_PORT']),
  database: process.env['PG_DBNAME'],
  max: 1,
  ssl: {
    rejectUnauthorized: false,
    ca: fs.readFileSync('./vultr-ca-certificate.crt').toString(),
  },
  archiveCompletedAfterSeconds: 60 * 5,
});
const queueName = 'company-queue';
const isWorker = !process.env['NOT_WORKER'];

const failedGoogleTypes = /\b(suburb|city|locality|office\b)/i
const softFailedGoogleTypes = /\b(organization|organisation\b)/i

const defaultBossSendOptions = { onComplete: true, expireInSeconds: 180 };

boss.on('error', error => console.error(error));

// prepare boss database, begin job monitoring
await boss.start();

console.log('[DEBUG] Connected to database successfully!');

let i = 0;
let j = 0;
let workerConsecutiveFails = 0;

const startTime = Math.round(Date.now() / 1000);

let itemsAdded = [];
let processedItems = {}

// monitor queue
if (isWorker) {
  console.log('Detected as worker.');
  await boss.work(queueName, {
    newJobCheckInterval: 750,
  }, someAsyncJobHandler);
} else {
  console.log('Detected as NOT a worker.');
  boss.onComplete(queueName, {
    teamSize: 250,
    newJobCheckIntervalSeconds: 5,
  }, onComplete);

  setInterval(() => {
      fs.writeFileSync('/home/ubuntu/FINAL/geo-api-responses-2.json', JSON.stringify(processedItems));
      console.log(`[FILE] Wrote to file, added: (${itemsAdded.length})` + JSON.stringify(itemsAdded));
      itemsAdded = [];
  }, 30000);

  setInterval(() => {
    console.log(`[INFO] Jobs collection rate: ${i / (Math.round(Date.now() / 1000.0) - startTime + 0.001)}`)
  }, 10000);

  const dataInput = JSON.parse(fs.readFileSync('/home/ubuntu/FINAL/geo-api-redo-input.json').toString());
  const dataInputLength = Object.keys(dataInput).length;
  console.log('[DEBUG] Loaded geo-api-input.json');

  const batches = _.chunk(Object.entries(dataInput), 750)
    .map((batch) => batch.map(([k, v]) => ({
      name: queueName,
      data: {
        id: k,
        company: v[3],
        area: v[2],
        state: v[1],
      },
      ...defaultBossSendOptions,
    })));

  for (const [index_batch, batch] of batches.entries()) {
    j += batch.length;

    await boss.insert(batch);
    console.log(`Sent batch #${index_batch}/${batches.length} (total = ${j}/${dataInputLength})`);
    await sleep(10000);
  }

  console.log('[DEBUG] All batches done!')
}

function onComplete(job) {
  // console.log(job.data);

  if (!job.data.failed) {
    itemsAdded.push(job.data.request.data.id);
    processedItems[job.data.request.data.id] = job.data.response;
  } else {
    console.log(`Failed job: ${job.data.request.data.id}`);
  }
}

async function someAsyncJobHandler(job) {
  let completed = false;
  const allLogs = [];
  const searchedVia = [];

  try {
    if (job.data.company) {
      let listedIndustries = [];
      let failedGoogleSearch = false;

      // Step 1: Google the word
      // let googleQuery = `${job.data.company} ${job.data.area?.trim() || ''} ${job.data.state?.trim() || ''}`.trim();
      let googleQuery = `${job.data.company} ${job.data.state?.trim() || ''}`.trim();
      allLogs.push(`Produced query: ${googleQuery}`);

      const result1 = await sites.google(googleQuery, allLogs);

      // Step 1a: If found to be a location, then ignore it
      if (result1.type && !failedGoogleTypes.exec(result1.type)) {
        listedIndustries.push(result1.type);
        allLogs.push(`Found type from Google: ${result1.type}`);
      } else if (softFailedGoogleTypes.exec(result1.type)) {
        listedIndustries.push(result1.type);
        failedGoogleSearch = true;
      } else {
        failedGoogleSearch = true;
      }

      // Step 2: Search for ABN using Google
      if (!job.data.area && !job.data.state && listedIndustries.length > 0) {
        googleQuery = `${googleQuery} ${listedIndustries[0]}`.trim();
      }

      const result2Base = (await sites.abn_from_name({ query: googleQuery, originalQuery: job.data.company }, allLogs))
      const result2 = result2Base?.chosenABR;
      // const candidateABNs = result2Base?.candidateABNList;

      let entityType = null;
      let entityName = null;
      let entityState = null;
      let entityABN = null;
      allLogs.push(`ABN query status: ${result2.status}`);
      if (result2.status === 'success') {
        entityABN = result2.abn;
        entityType = result2.entityType;
        entityName = result2.name;
        entityState = result2.location.stateCode;
        allLogs.push([entityABN, entityType, entityName, entityState]);
      }

      // Step 3: If Google couldn't find its type, go ahead with searching via DNB
      let failedDun = false;
      if (result2.status === 'success' && failedGoogleSearch) {
        const result3 = await sites.dunbradstreet({
          query: entityName,
          location: {
            state: entityState,
          },
        }, allLogs);
        
        searchedVia.push('dnb');
        listedIndustries.push(...(result3?.chosenResult?.industry || []));
        failedDun = result3?.chosenResult?.industry && result3.chosenResult.industry.some((idn) => softFailedGoogleTypes.exec(idn));
      }

      // Step 4: If Dunbrad doens't give enough specific results
      if (failedDun) {
        const result4 = await sites.yellow_pages({
          query: job.data.company,
          location: `${job.data.area?.trim() || ''} ${job.data.state?.trim() || ''}`.trim(),
        }, allLogs);
        
        searchedVia.push('yp');
        if (result4?.yp?.allCategories && result4.yp.allCategories.length > 0) {
          const allYPIndustries = Object.keys(result4.yp.allCategories);
          const bestYPCategory = allYPIndustries.reduce((a, b) => result4.yp.allCategories[b] > result4.yp.allCategories[a] ? b : a, result4.yp.allCategories[allYPIndustries[0]]);
          listedIndustries.push(bestYPCategory);
        }
      }

      const finalResponse = {
        entityName,
        entityABN,
        chosenABR: result2,
        entityType,
        searchedVia,
        listedIndustries,
      };

      allLogs.push(finalResponse);

      completed = true;

      await boss.complete(job.id, {
        completed,
        ...finalResponse
      });

    } else if (job.data.location) {
      // Step 1: Google the word
      let googleQuery = `${job.data.location?.trim() || ''} ${job.data.state?.trim() || ''}`.trim();
      allLogs.push(`Produced query: ${googleQuery}`);

      const result1 = await sites.google(googleQuery, allLogs);
      completed = true;

      await boss.complete(job.id, {
        completed,
        type: result1.type
      });

      workerConsecutiveFails = 0;
    } else {
      workerConsecutiveFails++;
      await boss.fail(job.id);
    }

  } catch (e) {
    console.error(e);
    workerConsecutiveFails++;
    await boss.fail(job.id, e);

    if (workerConsecutiveFails >= 5) {
      process.exit(-1);
    }
  }

  console.log(JSON.stringify([job.id, job.data.type, completed, allLogs]));
}
