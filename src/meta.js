import fs from 'fs';
import path from 'path';

const DATA_PATH = './config';

const META_INFO = JSON.parse(fs.readFileSync(path.resolve(DATA_PATH, 'meta.json')));

const API_PORT = parseInt(process.env.API_PORT || 3000);

export {
  META_INFO,
  DATA_PATH,
  API_PORT
}