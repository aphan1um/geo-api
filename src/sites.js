import cheerio from 'cheerio';
import querystring from 'querystring';
import fs from 'fs';
import leven from 'leven';
import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import AdblockerPlugin from 'puppeteer-extra-plugin-adblocker';
import soap from 'soap';
import pg from 'pg';
import BiMap from 'bidirectional-map';

import { DATA_PATH } from './meta.js'

import { get_webpage, get_puppeter, sleep, appendLog } from './util.js'
import path from 'path';

import _ from 'lodash';
import { fuzzy, search } from 'fast-fuzzy';

import Xvfb from 'xvfb';

puppeteer.use(AdblockerPlugin({ blockTrackers: true }));
puppeteer.use(StealthPlugin());

const IS_PROD = process.env.IS_PROD != null;

let browser;

if (IS_PROD) {
  const xvfb = new Xvfb({
    silent: false,
    timeout: 5000,
    xvfb_args: ["-screen", "0", '1920x1080x24+32', "-ac"],
  });

  console.log('[DEBUG] Starting xvfb\n');
  xvfb.startSync();

  // start puppeter
  browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox', '--start-fullscreen', '--incognito', '--display=' + xvfb._display],
    headless: false,
    defaultViewport: false,
  });
} else {
  // local run
  browser = await puppeteer.launch({
    args: ['--no-sandbox', '--window-size=2296,1541'],
    headless: false,
  });
}

console.log('[DEBUG] Browser started successfully');


const pool = new pg.Pool({
  host: process.env.PG_HOST,
  database: process.env.PG_DBNAME,
  user: process.env.PG_USERNAME,
  password: process.env.PG_PASSWORD,
  port: Number(process.env.PG_PORT)
});


const ABR_SOAP_URL = 'https://abr.business.gov.au/ABRXMLSearch/AbrXmlSearch.asmx?WSDL';
const ABR_GUID = '04eb6c69-0d25-4060-b446-af566247a0f0';
const ABN_CHARITY_DESCRIPTIONS = ["Charity", "Health Promotion Charity", "Public Benevolent Institution"];


const STATES_MAPPING = new BiMap(JSON.parse(fs.readFileSync(path.resolve(DATA_PATH, 'states-mapping.json'))));

const DUNBRAD_MAX_SEARCH = 2;

// TODO: IMPROVE THIS DATE CHECK
const YEAR = 1000 * 60 * 60 * 24 * 365;

const ABN_DATE_CHECK = (abnDetails) =>
  !(abnDetails.entityStatus.entityStatusCode.toLowerCase() == 'cancelled' && (new Date() - abnDetails.entityStatus.effectiveFrom) > YEAR);



let browserPage = null;

const abn_from_name = async (body, all_logs) => {
  // find abns in first page of google search
  const abnsList = await google_abn(body, all_logs);
  let foundABR;

  const potentialABNS = [];

  // if Google can get its ABN, we use that
  if (abnsList.length > 0) {
    for (const abn of abnsList) {
      appendLog(`>> Going through Google ABN: ${abn}`, all_logs);
      const abnDetails = await abnNumberDetailed(abn, all_logs);
      // go through every found google abn until we can get its state+postcode
      if (abnDetails.location.postcode && abnDetails.location.stateCode.length > 0) { // && ABN_DATE_CHECK(abnDetails)) {
        // foundABR = abnDetails;
        potentialABNS.push(abnDetails);
      } else {
        appendLog(`> Google ABN ${abn} skipped due to mission location `, all_logs);
      }

      await sleep(300 * Math.random() + 10);
      if (potentialABNS.length >= 3) {
        break;
      }
    }
  }

  if (potentialABNS.length > 0) {
    const abrSorted = search(body.originalQuery, potentialABNS, { keySelector: (obj) => obj.name, threshold: 0.0, useDamerau: false });
    foundABR = abrSorted[0];
  }

  // otherwise, directly use ABR API to get its ABN details by searching its name
  if (!foundABR) {
    foundABR = await abnDetailedFromName(body.query, null, all_logs);
  }

  return {
    candidateABNList: abnsList,
    chosenABR: foundABR
  };
}

const dnb_with_postcode = async (name, postcode, all_logs) => {
  // get location details based on postcode
  const dbQuery = await pool.query(
    'SELECT DISTINCT ssc_name, state_name FROM infoareas WHERE poa_code = $1', [postcode]);

  const locationSuburb = (dbQuery.rowCount > 0) ? dbQuery.rows[0].ssc_name : null;
  const locationState = (dbQuery.rowCount > 0) ? dbQuery.rows[0].state_name : null;

  all_logs.push(`> Found details for postcode ${postcode}: ${locationSuburb} ${locationState}`);

  // use dunbradstreet
  return await dunbradstreet({
    query: name,
    location: {
      suburb: locationSuburb,
      state: locationState,
    }
  }, all_logs);
}

const google_abn = async (body, all_logs) => {
  // search
  const search_query = {
    q: `${body.query} abn`
  };

  const new_url = `https://www.google.com.au/search?${querystring.stringify(search_query)}`;
  await openBrowserPage();
  browserPage.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36');

  let $;
  try {
    await get_puppeter(new_url, browserPage, all_logs, 'domcontentloaded');
    const dataHTML = await browserPage.evaluate(() => document.querySelector('*').outerHTML);
    $ = cheerio.load(dataHTML);
  } catch (e) {
    throw e;
  } finally {
    await closeBrowserPage();
  }

  let abnLinks = $("a[href*='abr.business.gov.au']")
    .map((_, el) => el.attribs['href'])
    .get();

  appendLog(abnLinks, all_logs);

  abnLinks = abnLinks.filter((href) => /^https?:\/\//i.test(href))
    .map((href) => new URL(href))
    .filter((url) => url.hostname.toLowerCase().endsWith('abr.business.gov.au') && url.pathname.toLowerCase().startsWith('/abn'))
    .map((url) => {
      return {
        url: url.href,
        abn: Number(decodeURI(url.pathname.substring(url.pathname.lastIndexOf('/') + 1)).replaceAll(' ', ''))
          || Number(decodeURI(url.searchParams.get('id')).replaceAll(' ', ''))
          || Number(decodeURI(url.searchParams.get('abn')).replaceAll(' ', ''))
      }
    });

  appendLog(abnLinks, all_logs);

  fs.writeFileSync('./temp-abn.html', $.html());

  // get first found abn from google's list of search results
  return abnLinks.map((el) => el.abn);
};

const google = async (queryString, all_logs) => {
  // search
  let search_query = {
    q: queryString
  };
  const new_url = `https://www.google.com.au/search?${querystring.stringify(search_query)}`;
  await openBrowserPage();
  // browserPage.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36');

  let $;
  try {
    await get_puppeter(new_url, browserPage, all_logs, 'domcontentloaded');
    const dataHTML = await browserPage.evaluate(() => document.querySelector('*').outerHTML);
    $ = cheerio.load(dataHTML);
  } catch (e) {
    throw e;
  } finally {
    await closeBrowserPage();
  }

  const businessPanel = $('div.osrp-blk')
  const businessText =
    $(businessPanel).find('div[data-attrid="kc:/local:one line summary"]').text() ||
    $(businessPanel).find('div[data-attrid="subtitle"] span').text();
  const businessName = $(businessPanel).find('[data-attrid="title"]').text();

  const businessAddress = $(businessPanel).find('div[data-attrid="kc:/location/location:address"]').text()?.replace(/^address: /i, '');
  let businessState, businessPostcode;

  if (businessAddress && businessAddress.length > 0) {
    [businessState, businessPostcode] = businessAddress.match(/([A-Z]+) (\d+)$/)?.slice(1) || [null, null];
  }

  const businessDescription =
    $(businessPanel).find('div[data-attrid="description"] span').text()
  const businessURL = $(businessPanel).find('a[data-attrid="visit_official_site"]').attr('href');

  const businessType =
    businessText.split(" in ")[0].trim().replace(/^\$+/, '');  // $$Pub -> Pub

  return {
    name: businessName,
    description: businessDescription,
    site_link: businessURL,
    type: (businessType.length > 0) ? businessType : null,
    address: {
      raw: (businessAddress && businessAddress.length > 0) ? businessAddress : null,
      state: businessState,
      postcode: (businessPostcode) ? Number(businessPostcode) : null,
    },
    href: new_url,
  };
};


const yellow_pages = async (body, all_logs) => {
  const search_query = {
    clue: body.query,
    locationClue: body.location
  };

  await openBrowserPage();

  await get_puppeter(
    `https://www.yellowpages.com.au/search/listings?${querystring.stringify(search_query)}`,
    browserPage,
    all_logs
  );

  const searchBody = await browserPage.evaluate(() => document.querySelector('*').outerHTML);
  const jsData = /window\.__INITIAL_STATE__\s*=\s*({.*})/.exec(searchBody)[1];
  let searchResults = JSON.parse(jsData)['model']['inAreaResultViews'];

  // sort based on data
  // searchResults.sort((e1, e2) => {
  //   const e1_leven = leven(e1.name.toLowerCase(), body.query.toLowerCase()) / Math.max(e1.name.length, body.query.length);
  //   const e2_leven = leven(e2.name.toLowerCase(), body.query.toLowerCase()) / Math.max(e2.name.length, body.query.length);

  //   if (e1_leven < e2_leven) {
  //     return -1;
  //   } else if (e2_leven < e1_leven) {
  //     return 1
  //   }

  //   return 0;
  // });

  searchResults = searchResults.filter((sr) => fuzzy(body.query.toLowerCase().trim(), sr.name.toLowerCase().trim()) >= 0.75)

  const allCategories = _.countBy(searchResults.map((s) => s.category.name.toLowerCase().trim()));

  // return searchResults[0].category.name;
  return {
    type: _.maxBy(Object.keys(allCategories), k => allCategories[k]) || null,
    yp: {
      allCategories,
      searchResults
    }
  };
}


const dunbradstreet = async (body, all_logs) => {
  let companiesFound = await dunbradstreet_search(body.query, body.location, all_logs);

  // filter for only Australian companies
  companiesFound = companiesFound.filter((c) => /\baustralia\b/.test(c.location.toLowerCase()));

  // now sort 
  const idealLocationText = `${STATES_MAPPING.getKey(body.location.state)}, Australia`.toLowerCase().trim();
  all_logs.push(`> Ideal dnb location string: ${idealLocationText}`);
  companiesFound.sort((c1, c2) => {
    const c1_leven = leven(c1.location.toLowerCase(), idealLocationText) / Math.max(c1.location.length, idealLocationText.length);
    const c2_leven = leven(c2.location.toLowerCase(), idealLocationText) / Math.max(c2.location.length, idealLocationText.length);

    if (c1_leven < c2_leven) {
      return -1;
    } else if (c2_leven < c1_leven) {
      return 1
    }

    return 0;
  });

  // now access best found result
  let queryResult;
  if (companiesFound.length > 0) {
    for (let i = 0; i < Math.min(DUNBRAD_MAX_SEARCH, companiesFound.length); i++) {
      queryResult = await dunbradstreet_industry(companiesFound[i].href, all_logs);
      if (queryResult) {
        break;
      }
      appendLog(`> [INFO] Search failed for: ${companiesFound[0].href}`, all_logs);
    }
  }

  return {
    companiesFound,
    chosenResult: queryResult,
  };
}

const dunbradstreet_industry_cleantext_re = /\s+,+/g;

const dunbradstreet_industry = async (url, all_logs) => {
  await openBrowserPage();
  browserPage.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36');

  let $;
  try {
    await get_puppeter(url, browserPage, all_logs, 'domcontentloaded');
    const dataHTML = await browserPage.evaluate(() => document.querySelector('*').outerHTML);
    $ = cheerio.load(dataHTML);
  } catch (e) {
    throw e;
  } finally {
    await closeBrowserPage();
  }

  if ($('body').attr('class') == 'error-page') {
    const error_title = $('h1.title').text().trim();
    const error_subtitle = $('p.subtitle').text().trim();
    appendLog(`${error_title}: ${error_subtitle}`, all_logs);

    if (error_subtitle.toLowerCase().indexOf('looks like this page no longer exists') >= 0 && error_title.indexOf("500") >= 0) {
      return null;
    } else {
      return new Error(`${error_title}: ${error_subtitle}`);
    }
  }

  let industries = [
    $('span.company_data_point[name="industry_links"] > span > span:not(.overview-view-more-industries-list) > :first-child'),
    $('span.company_data_point[name="industry_links"] > span.overview-view-more-industries-list > span > span > :first-child')
  ].map((res) => res.map((_, el) => $(el).text()).get())
    .flat()
    .map((txt) => txt.replaceAll(dunbradstreet_industry_cleantext_re, '').trim().toLowerCase())
    .filter((ind) => ind.length > 0);

  const address_raw = $('span.company_data_point[name="company_address"] > span').text()?.trim()?.replace(/See other locations$/i, '')?.trim();
  let address_postcode;
  let address_state;
  if (address_raw && address_raw.length > 0) {
    const scriptJSON = JSON.parse($('body > script[type="application/ld+json"]').html());
    appendLog(scriptJSON, all_logs);
    address_postcode = Number(scriptJSON.address.postalCode);
    address_state = scriptJSON.address.addressRegion;
  }

  return {
    industry: [... new Set(industries)],
    address: {
      raw: address_raw,
      postcode: address_postcode,
      state: address_state,
    },
    site_link: $('#hero-company-link').attr('href'),
  };
}


const dunbradstreet_search = async (query, location, all_logs) => {
  await openBrowserPage();
  browserPage.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36');

  let searchResults;
  try {
    await get_puppeter(
      `https://www.dnb.com/site-search-results.html#tab=Company%20Profiles&CompanyProfilesSearch=${encodeURI(query)}`,
      browserPage,
      all_logs
    );

    await sleep(450);
    await browserPage.$eval(".search_button", el => el.click());

    await browserPage.waitForSelector("[class*='resultsHeading']", { timeout: 5000 });

    const noResultsElement = await browserPage.$("div[class*='noResultBox']");
    if (noResultsElement) {
      throw new Error(`There were no results for query: '${query}'`);
    }

    await browserPage.waitForSelector("input[placeholder='Enter Country/Region']", { timeout: 5000 });
    await browserPage.focus("input[placeholder='Enter Country/Region']");
    await browserPage.keyboard.type("Australia");
    await sleep(250);
    await browserPage.keyboard.press('Enter');

    await sleep(450);

    searchResults = await browserPage.$$eval(
      'table > tbody > tr',
      els => els.map((el) => el.innerHTML)
    );

    // console.dir(searchResults);


  } catch (e) {
    appendLog({
      level: 'warn',
      stack: e.message,
    }, all_logs);

    // get number of search results
    const resultsCount = (await browserPage.$$('div[id=company_results] > ul > li.search_result')).length;
    appendLog(`Results count: ${resultsCount}`, all_logs);

    let bodyHTML = cheerio.load(await browserPage.evaluate(() => document.documentElement.outerHTML)).text();
    if (e.message.toLowerCase().includes('waiting for selector `li.option[value="au"]` failed')) {
      appendLog({
        level: 'warn',
        msg: 'It appears there is no Australian filter, skipping dunbrad search instead...'
      }, all_logs);
      return [];
      // TODO: bodyHTML check might be redundant now..
    } else if (!bodyHTML.includes('There are no search results') && resultsCount > 0) {
      appendLog({
        level: 'error',
        msg: 'Page does not seem to have failed due to no search results, abruptly ending request...',
      }, all_logs);
      throw e;
    } else {
      return [];
    }
  } finally {
    await closeBrowserPage();
  }

  let parsedResults = searchResults.map((html) => {
    const $ = cheerio.load(html);

    return {
      name: $("[class*='tableCompanyNameLink']").text(),
      href: 'https://www.dnb.com' + $('a').attr('href'),
      location: $("span[class*='tableCountryRegion']").text(),
      locationType: $("span[class*='tableLocationType']").text(),
      industry: $("span[class*='tableIndustryName']").text(),
    }
  });

  appendLog({
    listedCompanies: parsedResults,
    totalCompanies: searchResults.length
  }, all_logs);

  return parsedResults;
}

const getABNName = (abr_result, all_logs) => {
  let nameField = abr_result?.mainName ||
    abr_result?.businessName ||
    abr_result?.mainTradingName ||
    abr_result?.otherTradingName ||
    abr_result?.dgrFundName;

  appendLog({
    nameField: nameField
  }, all_logs);

  if (nameField) {
    return (nameField instanceof Array) ? nameField[0].organisationName : nameField.organisationName;
  } else {
    return abr_result.legalName.fullName ||
      `${abr_result.legalName.givenName} ${abr_result.legalName.otherGivenName} ${abr_result.legalName.familyName}`;
  }
};

const abnSearch = async ({ companyName, state }, all_logs) => {
  let params = {
    "name": companyName,
    "activeABNsOnly": "Y", // active ABNs so they can be searched by dunbrad
    "minimumScore": "92",
    "authenticationGuid": ABR_GUID,
    "maxSearchResults": "10",
  };

  if (state && state.trim().length > 0) {
    params[state.toUpperCase()] = 'Y';
  }

  const client = await soap.createClientAsync(ABR_SOAP_URL);
  const result = (await client.ABRSearchByNameAdvancedSimpleProtocol2017Async(params))[0];

  appendLog(result, all_logs);

  return result
    .ABRPayloadSearchResults.response?.searchResultsList?.searchResultsRecord
    ?.map(sr => {
      return {
        name: getABNName(sr, all_logs),
        abn: Number(sr.ABN[0].identifierValue),
        location: {
          stateCode: sr.mainBusinessPhysicalAddress[0].stateCode,
          postcode: Number(sr.mainBusinessPhysicalAddress[0].postcode),
        }
      }
    }) || [];
}

const abnNumberDetailed = async (abnNumber, all_logs) => {
  const isACNNumber = (abnNumber.toString().length <= 9);
  let ret;

  let params = {
    "searchString": (isACNNumber) ? String(abnNumber).padStart(9, '0') : abnNumber,
    "authenticationGuid": ABR_GUID,
    "includeHistoricalDetails": "N",
  };

  const client = await soap.createClientAsync(ABR_SOAP_URL);
  const result = (await (isACNNumber ? client.SearchByASICv201408Async : client.SearchByABNv202001Async)(params))[0];
  appendLog(result, all_logs);

  const relevantResult = result.ABRPayloadSearchResults.response[isACNNumber ? 'businessEntity201408' : 'businessEntity202001'];
  const charityType = relevantResult.entityType.entityTypeCode;
  let entityType = relevantResult.entityType.entityTypeCode;

  if (charityType) {
    const charityTypeDescription = charityType.charityTypeDescription;

    if (ABN_CHARITY_DESCRIPTIONS.includes(charityTypeDescription)) {
      appendLog(`> Got entity type for ABN ${abnNumber}: ${charityTypeDescription} (charity found)`, all_logs);
      entityType = 'nonprofit';
    }
  }

  ret = {
    status: 'success',
    entityType: entityType,
    entityStatus: relevantResult.entityStatus[0] || relevantResult.entityStatus,
    location: {
      stateCode: relevantResult.mainBusinessPhysicalAddress[0].stateCode,
      postcode: Number(relevantResult.mainBusinessPhysicalAddress[0].postcode),
    },
    name: getABNName(relevantResult, all_logs),
    abn: relevantResult.ABN[0]?.identifierValue || relevantResult.ABN.identifierValue,
    businessNames: relevantResult.businessName?.map((bn) => bn['organisationName']) || [],
    tradingNames: (relevantResult.mainTradingName?.map((bn) => bn['organisationName']) || []).concat(relevantResult.otherTradingName?.map((bn) => bn['organisationName']) || []),
  };

  appendLog({
    abnNumber,
    isACNNumber,
    entityType,
    charityType,
    result: ret
  }, all_logs);

  return ret;
}

const abnDetailedFromName = async (companyName, state, all_logs) => {
  const abnNumbers = await abnSearch({
    companyName,
    state
  }, all_logs);

  if (abnNumbers.length == 0) {
    return { status: 'noresult' };
  }

  appendLog({
    companyName,
    companiesFound: abnNumbers,
  }, all_logs);

  for (const abn of abnNumbers) {
    const abnDetails = await abnNumberDetailed(abn.abn, all_logs);

    if (abnDetails.entityType) {
      appendLog({ chosenABNResult: abnDetails }, all_logs);

      return {
        status: abnDetails.status,
        abn: abn.abn,
        name: abnDetails?.name || abn.name,
        location: abnDetails?.location || abn.location,
        entityType: abnDetails.entityType,
      };
    }
  }

  return { status: 'failure' };
}

async function openBrowserPage() {
  if (!browserPage) {
    browserPage = await browser.newPage();
  }
}

async function closeBrowserPage(chance = 0.1) {
  if (Math.random() < chance) {
    await browserPage.close();
    browserPage = null;
  }
}


export default {
  google,
  google_abn,
  yellow_pages,
  dunbradstreet,
  abnSearch,
  abnNumberDetailed,
  abnDetailedFromName,
  abn_from_name,
  dnb_with_postcode
};
