import pg from 'pg';
import fs from 'fs';
import path from 'path';

import { DATA_PATH } from './meta.js'

const pool = new pg.Pool({
  host: process.env.PG_HOST,
  database: process.env.PG_DBNAME,
  user: process.env.PG_USERNAME,
  password: process.env.PG_PASSWORD,
  port: Number(process.env.PG_PORT)
});

const statesFullNames = JSON.parse(fs.readFileSync(path.resolve(DATA_PATH, 'states-mapping.json')));
const postcodesException = JSON.parse(fs.readFileSync(path.resolve(DATA_PATH, 'postcode-exceptions.json')));

const reStrs = {
  statesFull: new RegExp(`\\b(${Object.keys(statesFullNames).join('|')})\\b`, 'i'),
  states: new RegExp(`(\\b${Object.values(statesFullNames).join('|')}\\b)`),
  postcode: /\b(\d{3,4})\b/,
  cleanText: /\b(area|suburb)s?\b/i,
}

const sqlQueryBestLoc = `
  SELECT area_id, ssc_name as value, similarity(ssc_name, $1) as sim, 'ssc_name' AS field FROM infoareas WHERE ssc_name % $1 AND state_name = $2 UNION
  SELECT area_id, sa4_name, similarity(sa4_name, $1), 'sa4_name' FROM infoareas WHERE sa4_name % $1 AND state_name = $2 UNION
  SELECT area_id, sa3_name, similarity(sa3_name, $1), 'sa3_name' FROM infoareas WHERE sa3_name % $1 AND state_name = $2 UNION 
  SELECT area_id, sa2_name, similarity(sa2_name, $1), 'sa2_name' FROM infoareas WHERE sa2_name % $1 AND state_name = $2
  ORDER BY sim DESC, field DESC 
`;

const sqlQueryExactLoc = `
  SELECT
    area_id,
    poa_code as value, GREATEST(similarity(ssc_name, $3), similarity(sa4_name, $3), similarity(sa3_name, $3), similarity(sa2_name, $3)) as sim,
    'poa_code' AS field
  FROM infoareas WHERE poa_code = $1 and state_name = $2
  ORDER BY sim DESC
`;

const sqlQueryExactLocPOAOnly = `
  SELECT area_id, poa_code as value, 1 as sim, 'poa_code' AS field, state_name FROM infoareas WHERE poa_code = $1
`;

const getLocation = async (body, all_logs) => {
  if (typeof body.location == 'string') {
    if (body.location.length === 0) {
      body.location = guessLocationFromDescription(body.description);
    }

    return retrieveLocationFromString(body.location);
  } else {
    let txt = body.location.suburb || body.location.area || body.location.city;

    if (txt.length === 0) {
      txt = guessLocationFromDescription(body.description);
    }

    if (body.location.nation) {
      console.assert(body.location.nation.toLowerCase().trim() == 'australia', 'Received non-Australian nation value');
    }

    return retrieveLocationFromString(`${txt} ${body.location.state}`);
  }
}

const guessLocationFromDescription = (description) => {
  let ret = ""

  const stateNames = Object.keys(statesFullNames);
  const capitalCityNames = Object.keys(postcodesException);
  const tokenised = description.toLowerCase().split(' ').map((el) => el.replace(/[\s\.]+$/, ''));

  const tokenisedCapitalCity = tokenised.filter((el) => capitalCityNames.includes(el));
  const tokenisedState = tokenised.filter((el) => stateNames.includes(el));

  
  if (tokenisedState?.length === 1) ret += `${tokenisedState[0]} `;
  if (tokenisedCapitalCity?.length == 1) ret += tokenisedCapitalCity[0];
  return ret.trim();
}


const retrieveLocationFromString = async (text) => {
  let tempText = text;
  let locSims;
  let inferred = false;
  tempText = tempText.replace(reStrs.statesFull, (m) => statesFullNames[m.toLowerCase()]);

  // get Australian state
  let state = reStrs.states.exec(tempText)?.[1];
  tempText = tempText.replace(reStrs.states, '').trim().toLowerCase();

  // get postcode
  let postcode = reStrs.postcode.exec(tempText)?.[1];
  tempText = tempText.replace(reStrs.postcode, '').trim();

  if (!postcode && Object.keys(postcodesException).includes(tempText)) {
    postcode = postcodesException[tempText];
    inferred = true;
  }

  tempText = tempText.replace(reStrs.cleanText, '').trim();

  if (state) {
    if (postcode) {
      locSims = (await pool.query(sqlQueryExactLoc, [Number(postcode), state, tempText])).rows;
    } else {
      locSims = (await pool.query(sqlQueryBestLoc, [tempText, state])).rows;
    }
  } else {
    if (postcode) {
      locSims = (await pool.query(sqlQueryExactLocPOAOnly, [Number(postcode)])).rows;
      state = locSims[0].state_name; // TODO: Rough fix
    }
  }

  return {
    info: {
      state,
      postcode,
      inferred,
      isEmpty: !(state && postcode)
    },
    text,
    tempText,
    locSims
  };
}

export default {
  getLocation,
}