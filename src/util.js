import axios from 'axios';

import { META_INFO } from './meta.js'


const cooldowns = META_INFO['site_cooldowns']

const cooldowns_next = {
};


const ALL_AUS_STATES = {
  WA: 'Western Australia'
}

Object.keys(cooldowns).forEach((site) => cooldowns_next[site] = Date.now());

const get_webpage = async (url, headers, all_logs) => {
  console.log(`> Accessing page: ${url}`);
  await wait_for_cooldown(new URL(url).origin, all_logs);

  const result = await axios.get(url, {
    timeout: 5000,
    headers
  });

  // TODO: TEMP WORK
  // fs.writeFileSync('./temp.html', result.data);
  return result.data;
}

const wait_for_cooldown = async (site, all_logs) => {
  while (Date.now() < cooldowns_next[site]) {
    appendLog(`Waiting for ${cooldowns_next[site] - Date.now()} ms...`, all_logs);
    await sleep(cooldowns_next[site] - Date.now());
  }

  const delayAdd = Math.floor((cooldowns[site][1] - cooldowns[site][0]) * Math.random() + cooldowns[site][1]);
  cooldowns_next[site] = Date.now() + delayAdd;
}

const sleep = (ms) => {
  if (ms < 0) return;

  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

const get_puppeter = async (url, browserPage, all_logs, waitUntil = 'networkidle2') => {
  appendLog(`> Accessing page (Puppeteer): ${url}`, all_logs);
  await wait_for_cooldown(new URL(url).origin, all_logs);
  return browserPage.goto(url, { waitUntil });
}

const get_aus_state_fullname = async (stateCode) => { // VIC, NSW etc.
  for (const [key, value] of ALL_AUS_STATES.entries()) {
    if (stateCode.toLowerCase() == key.toLowerCase()) {
      return value;
    }
  }
}

const appendLog = (msg, all_logs) => {
  all_logs.push(msg);
}

export {
  get_webpage,
  get_puppeter,
  sleep,
  get_aus_state_fullname,
  appendLog
};